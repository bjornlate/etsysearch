package com.blackberry.store.service;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.squareup.okhttp.OkHttpClient;

public class JsonService {
	private static int NUMBER_OF_CORES = Runtime.getRuntime()
			.availableProcessors();

	private OkHttpClient client;
	private BlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>();
	private ThreadPoolExecutor pool = new ThreadPoolExecutor(1, NUMBER_OF_CORES, 10, TimeUnit.SECONDS, queue);

	public JsonService(OkHttpClient client) {
		// Maybe this guy should create his own http client so that
		// images don't push json data out of cache?
		this.client = client;
	}

	public void fetch(String url, JsonCallback callback) {
		JsonTask runnable = new JsonTask(client, url, callback);
		pool.execute(runnable);
	}

}
