package com.blackberry.store.service;

import org.json.JSONObject;


public abstract class JsonCallback {
    public abstract void callback(boolean isError, JSONObject jsonObject, Exception exception);
}
