package com.blackberry.store.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.res.AssetManager;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.blackberry.store.Constants;
import com.blackberry.store.StoreApplication;
import com.squareup.okhttp.OkHttpClient;

class JsonTask extends Handler implements Runnable {
	private OkHttpClient client;
	private String urlString;
	private JSONObject jsonObject;
	private JsonCallback callback;

	public JsonTask(OkHttpClient client, String urlString, JsonCallback callback) {
		this.client = client;
		this.urlString = urlString;
		this.callback = callback;
	}

	@Override
	public void run() {
		try {
			URL url = new URL(urlString);
			if ("file".equals(url.getProtocol())) {
				jsonObject = getFile(url);
			} else {
				jsonObject = getHttp(url);
			}
			Message completeMessage = obtainMessage(0, this);
			completeMessage.sendToTarget();
		} catch (JSONException e) {
			e.printStackTrace();
			Message errorMessage = obtainMessage(1, e);
			errorMessage.sendToTarget();
		} catch (IOException e) {
			e.printStackTrace();
			Message errorMessage = obtainMessage(1, e);
			errorMessage.sendToTarget();
		}
	}

	private JSONObject getFile(URL url) throws JSONException, IOException {
		AssetManager am = StoreApplication.applicationContext().getAssets();
		String path = urlString.replaceFirst("file://assets/", "");
		Log.d(Constants.TAG_STORE, "Opening JSON: " + path);
		InputStream in = am.open(path, AssetManager.ACCESS_STREAMING);
		byte[] buffer = readFully(in);
		JSONObject jsonObject = new JSONObject(new String(buffer));
		return jsonObject;
	}

	// TODO check response code
	// TODO check deny header
	// TODO check store config version
	private JSONObject getHttp(URL url) throws JSONException, IOException {
		HttpURLConnection connection = client.open(url);
		InputStream in = null;
		try {
			// Read the response.
			in = connection.getInputStream();
			byte[] buffer = readFully(in);
			JSONObject jsonObject = new JSONObject(new String(buffer));
			return jsonObject;
		} finally {
			if (in != null)
				in.close();
		}
	}

	private byte[] readFully(InputStream in) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int read = 0;
		byte[] buffer = new byte[1024];
		while (read > -1) {
			read = in.read(buffer);
			if (read > 0) {
				out.write(buffer, 0, read);
			}
		}
		return out.toByteArray();
	}

	@Override
	public void handleMessage(Message msg) {
		switch (msg.what) {
		case 0:
			callback.callback(false, jsonObject, null);
			break;
		case 1:
			callback.callback(true, null, (Exception) msg.obj);
		}
	}
}