package com.etsysearch;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.etsysearch.net.EtsyJsonHttpCallback;
import com.etsysearch.net.EtsyJsonHttpClient;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.support.v4.app.ListFragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * This class Retrieves content from the Etsy listing search API, which is returned as JSON:
 * https://api.etsy.com/v2/listings/active?api_key=liwecjs0c3ssk6let4p1wqt9&includes=MainImage&keywords=<keywords>
 * 
 * This class supports keyword search as well as fetching additional search result pages.
 * 
 * Multiple search queries can be made in parallel.
 * The response that matches the currentSearchQuery is the one that is appended into the adapter's array.
 * 
 * As the user scrolls the next page is requested when the adapter creates a view for the item half a page from the 
 * current end.
 */
public class EtsyJsonArrayAdaptor extends ArrayAdapter<JSONObject> {

	private ListFragment listFragment;
	
	private ArrayList<JSONObject> pages = new ArrayList<JSONObject>();

	private int effectiveLimit;
	private int nextPage;
	private int resultsCount;
	//private int effectivePage;
	
	private String categoryPath = "";
	private String currentSearchQuery = "";
	
	private final static String URL_LISTING_SEARCH = "https://api.etsy.com/v2/listings/active?api_key=liwecjs0c3ssk6let4p1wqt9&includes=MainImage&keywords=";

	private boolean isPaging = false;

	
	public EtsyJsonArrayAdaptor(Context context, int resource) {
		super(context, resource);
	}

	public String getCurrentSearchQuery() {
		return currentSearchQuery;
	}
	
	public void searchRefresh(){
		search(currentSearchQuery);
	}

	public void setCategoryPath(String string) {
		this.categoryPath = string;
	}

	public CharSequence getCategoryPath() {
		return categoryPath;
	}

	public void setListFragment(ListFragment listFragment) {
		this.listFragment = listFragment;
	}
	
	public View getView(int position, View convertView,
			ViewGroup parent) {
		View view = convertView;
		if(convertView == null){
			view = LayoutInflater.from(getContext()).inflate(R.layout.view_etsy_row_item_layout, null);
		}
		
		
		JSONObject itemJson = getItem(position);
		
		TextView tv = (TextView) view.findViewById(R.id.ssli_title);
		if(tv != null){
			String title = itemJson.optString("title", "<title>");
			tv.setText(Html.fromHtml(title));
		}
		
		/*
		 * if a request for an item half a page from the end of the list is 
		 * made, get the next page. This way it should arrive close to when the
		 * user reaches the current end, appending more results.
		 */
		if((getCount() - position) == effectiveLimit/2){
			fetchNextPage();
		}
		
		tv = (TextView) view.findViewById(R.id.ssli_subtitle);
		if(tv != null){
			String title = "$" + itemJson.optString("price", "<price>");
			tv.setText(Html.fromHtml(title));
		}
		
		tv = (TextView) view.findViewById(R.id.ssli_categories);
		if(tv != null){
			JSONArray categories = itemJson.optJSONArray("category_path");
			if(categories != null){

				StringBuffer categoriesStr = new StringBuffer();
				for(int i=0;i<categories.length();++i){
					categoriesStr.append(categories.optString(i));
					if(i < categories.length() -1){
						categoriesStr.append(" | ");
					}
				}
				
				tv.setText(Html.fromHtml(categoriesStr.toString()));
			}
		}
		
		
		ImageView icon = (ImageView) view.findViewById(R.id.ssli_icon);
		if(icon != null){
			JSONObject mainImage = itemJson.optJSONObject("MainImage");
			if(mainImage != null){
				String iconUrl = mainImage.optString("url_170x135");
				if(!iconUrl.isEmpty()){
					Picasso.with(getContext()).load(iconUrl).into(icon);
				}
			}
		}	
		return view;
	}
	
	private String buildUrl(int page){
		StringBuffer sb = new StringBuffer();
		sb.append(URL_LISTING_SEARCH);
		try{
			sb.append(URLEncoder.encode(currentSearchQuery, "utf-8"));
			
			if(page > 0){
				sb.append("&page=");
				sb.append(page);
			}
			
			if(!categoryPath.isEmpty()){
				sb.append("&category=");
				sb.append(URLEncoder.encode(categoryPath, "utf-8"));
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	/**
	 * Search code
	 */
	
	protected void search(final String query){
		
		//Clear the adapter and the list of pages
		currentSearchQuery = query;
		clear();
		pages.clear();
		
		if(query.length() < 3){
			//don't bother searching for this term, its not a full word.
			Log.i("INFO", "Skipping search for this term, less than 3 chars: " + query);
			notifyDataSetChanged();
			return;
		}

		if(listFragment != null){
			listFragment.setListShown(false);
		}
		
		EtsyJsonHttpClient client = EtsyJsonHttpClient.getInstance();

		String url = buildUrl(0);
		Log.i("search", url);
		client.fetch(url, new EtsyJsonHttpCallback() {
			String searchQuery = query;
			@Override
			public void callback(boolean isError, JSONObject jsonObject, Exception exception) {
				//Log.i("YAY", jsonObject.toString());
				Log.i("callback", "Response received");
				
				/**
				 * Ignore responses that are no longer up to date.
				 */
				if(searchQuery.equals(currentSearchQuery)){
					pages.add(jsonObject);
					
					updatePaginationData(jsonObject);
					
					updateContent();
				}
			}
		});
	}
	
	/**
	 * Pagination Code
	 */
	public void fetchNextPage(){
		
		if(isPaging){
			
			Log.i("fetchNextPage", "Busy searching, ignoring page request");
			return;
			
		}else if(resultsCount > getCount()){
			/*
			 * if the total results are greater than the current size of the adapter array,
			 * get the next page.
			 */
			isPaging = true;
			EtsyJsonHttpClient client = EtsyJsonHttpClient.getInstance();;
			
			
			String url = buildUrl(nextPage);
			Log.d("fetchNextPage", "url: "+url);
			client.fetch(url, new EtsyJsonHttpCallback() {
				@Override
				public void callback(boolean isError, JSONObject jsonObject, Exception exception) {
					//Log.i("YAY", jsonObject.toString());
					Log.i("callback", "Response received");
					if(!isError){
						pages.add(jsonObject);
						updatePaginationData(jsonObject);
					}
					updateContent();
					isPaging = false;
				}
			});
		}
		
	}
	
	private void updatePaginationData(JSONObject jsonObject){
		if(jsonObject != null){
			JSONObject paginationData = jsonObject.optJSONObject("pagination");
			if(paginationData != null){
				effectiveLimit = paginationData.optInt("effective_limit");
				nextPage= paginationData.optInt("next_page");
				resultsCount = jsonObject.optInt("count");
				//effectivePage = paginationData.optInt("effective_page");
			}
		}
	}
	
	
	/**
	 * Adapter update.
	 */
	
	private void updateContent(){
		if(pages.size() > 0){
			
			if(listFragment != null){
				listFragment.setListShown(true);
			}

			JSONArray results = pages.get(pages.size()-1).optJSONArray("results");
			if(results != null){
				for(int i=0;i<results.length(); ++i){
					JSONObject jsonObject = results.optJSONObject(i);
					if(jsonObject != null){
						add(jsonObject);
					}
				}
			}
			notifyDataSetChanged();
		}
	}



}
