package com.etsysearch;

import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

/**
 * A list fragment representing a list of SearchResults. This fragment also
 * supports tablet devices by allowing list items to be given an 'activated'
 * state upon selection. This helps indicate which item is currently being
 * viewed in a {@link SearchResultDetailFragment}.
 * <p>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class SearchResultListFragment extends ListFragment implements TextWatcher {

	
	EtsyJsonArrayAdaptor etsyJsonArrayAdapter;
	
	/**
	 * The serialization (saved instance state) Bundle key representing the
	 * activated item position. Only used on tablets.
	 */
	private static final String STATE_ACTIVATED_POSITION = "activated_position";

	private Callbacks callbacks;

	/**
	 * The current activated item position. Only used on tablets.
	 */
	private int mActivatedPosition = ListView.INVALID_POSITION;

	/**
	 * A callback interface that all activities containing this fragment must
	 * implement. This mechanism allows activities to be notified of item
	 * selections.
	 */
	public interface Callbacks {
		/**
		 * Callback for when an item has been selected.
		 */
		public void onItemSelected(String id);
	}


	public SearchResultListFragment() {
	}

	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.layout_searchable_list, container);
		
		EditText searchText = (EditText) view.findViewById(R.id.search_text);
		if(searchText != null){
			searchText.addTextChangedListener(this);
		}
		
		return view;
	}



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		etsyJsonArrayAdapter = new EtsyJsonArrayAdaptor(getActivity(), R.layout.view_etsy_row_item_layout);
		etsyJsonArrayAdapter.setListFragment(this);
		setListAdapter(etsyJsonArrayAdapter);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		// Restore the previously serialized activated item position.
		if (savedInstanceState != null
				&& savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
			setActivatedPosition(savedInstanceState
					.getInt(STATE_ACTIVATED_POSITION));
		}
		
		registerForContextMenu(getListView());
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// Activities containing this fragment must implement its callbacks.
		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException(
					"Activity must implement fragment's callbacks.");
		}

		callbacks = (Callbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();

		// Reset the active callbacks interface to the dummy implementation.
		callbacks = null;
	}

	@Override
	public void onListItemClick(ListView listView, View view, int position,
			long id) {
		super.onListItemClick(listView, view, position, id);

		// Notify the active callbacks interface (the activity, if the
		// fragment is attached to one) that an item has been selected.
		if(callbacks != null){
			callbacks.onItemSelected(etsyJsonArrayAdapter.getItem(position).toString());
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mActivatedPosition != ListView.INVALID_POSITION) {
			// Serialize and persist the activated item position.
			outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
		}
	}

	/**
	 * Turns on activate-on-click mode. When this mode is on, list items will be
	 * given the 'activated' state when touched.
	 */
	public void setActivateOnItemClick(boolean activateOnItemClick) {
		// When setting CHOICE_MODE_SINGLE, ListView will automatically
		// give items the 'activated' state when touched.
		getListView().setChoiceMode(
				activateOnItemClick ? ListView.CHOICE_MODE_SINGLE
						: ListView.CHOICE_MODE_NONE);
	}

	private void setActivatedPosition(int position) {
		if (position == ListView.INVALID_POSITION) {
			getListView().setItemChecked(mActivatedPosition, false);
		} else {
			getListView().setItemChecked(position, true);
		}

		mActivatedPosition = position;
	}
	
	@Override
	public void afterTextChanged(Editable s) {
		/**
		 * When the user has entered text, queue up a new auto search using a timer of 1 second.
		 * There's need to fire off multiple unnecessary search queries when the user has typed in a 
		 * word quickly.
		 */
		latestSearchQuery = s.toString().trim();
		
		searchTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				getActivity().runOnUiThread(new Runnable(){
					public void run(){
						if(!latestSearchQuery.equals(etsyJsonArrayAdapter.getCurrentSearchQuery())){
							etsyJsonArrayAdapter.search(latestSearchQuery);
						}
					}
				});
			}
		}, 1000);
	}
	String latestSearchQuery;
	Timer searchTimer = new Timer();
	
	
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}

	

	@Override
	public void setListShown(boolean shown) {
		
		Activity activity = getActivity();
		if(activity == null){
			Log.i("setListShown","activity has been destroyed, likely due to rotation event.");
			return;
		}
		
		View progressContainer = getActivity().findViewById(R.id.progressContainer);
		
		if(progressContainer == null){
			return;
		}
		
		ProgressBar pb = (ProgressBar) getActivity().findViewById(R.id.progressSpinner);
		
		if(pb == null){
			return;
		}
		
		ListView lv = this.getListView();
		if(lv == null){
			return;
		}
		
		
		if(shown){
			lv.setVisibility(View.VISIBLE);
			pb.setVisibility(View.INVISIBLE);
			progressContainer.setVisibility(View.INVISIBLE);
		}else{
			lv.setVisibility(View.INVISIBLE);
			progressContainer.setVisibility(View.VISIBLE);
			pb.setVisibility(View.VISIBLE);
		}
	}


	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
//		MenuInflater inflater = getActivity().getMenuInflater();
		//inflater.inflate(R.menu.record_context_menu, menu);
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
    	JSONObject item = etsyJsonArrayAdapter.getItem(info.position);
    	if(item != null){
    		JSONArray categories = item.optJSONArray("category_path");
    		
    		if(categories != null){

				for(int i=0;i<categories.length();++i){
					menu.add(Menu.NONE, i, Menu.NONE, "Show only items in "+categories.optString(i));
				}
    		}
    	}
	}



	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		JSONObject searchResult = etsyJsonArrayAdapter.getItem(info.position);
		
		JSONArray categories = searchResult.optJSONArray("category_path");
		if(categories != null){
			
//			StringBuffer sb = new StringBuffer();
//			for(int i=0;i<=item.getItemId();++i){
//				sb.append(categories.optString(i));
//				if(i!=item.getItemId()){
//					sb.append("\\");
//				}
//			}
//			
//			categoryPath = sb.toString();
			etsyJsonArrayAdapter.setCategoryPath(categories.optString(item.getItemId()));

			Button categoryFilterButton = (Button)getActivity().findViewById(R.id.category_filter_button);
			
			View explanation = getActivity().findViewById(R.id.category_explanation);
			explanation.setVisibility(View.VISIBLE);
			
			if(categoryFilterButton != null){
				categoryFilterButton.setText(etsyJsonArrayAdapter.getCategoryPath());
				categoryFilterButton.setVisibility(View.VISIBLE);
				categoryFilterButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						etsyJsonArrayAdapter.setCategoryPath("");
						v.setVisibility(View.GONE);
						etsyJsonArrayAdapter.searchRefresh();
						View explanation = getActivity().findViewById(R.id.category_explanation);
						explanation.setVisibility(View.GONE);
					}
				});
			}
			
			etsyJsonArrayAdapter.searchRefresh();
			return true;
		}
		
        return super.onContextItemSelected(item);
	}
	
}

