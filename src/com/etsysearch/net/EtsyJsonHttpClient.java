package com.etsysearch.net;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.net.http.HttpResponseCache;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class EtsyJsonHttpClient {
	
	private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
	
	private BlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>();
	private ThreadPoolExecutor pool = new ThreadPoolExecutor(1, NUMBER_OF_CORES, 10, TimeUnit.SECONDS, queue);

	private static Context context;
	
	public static void setContext(Context context){
		EtsyJsonHttpClient.context = context;
	}
	
	private EtsyJsonHttpClient() {
		try {
			/**
			 * Initialize the HTTP cache.
			 */
			if(context != null){
				File httpCacheDir = new File(context.getCacheDir(), "http");
				long httpCacheSize = 10 * 1024 * 1024; // 10 MiB
				HttpResponseCache.install(httpCacheDir, httpCacheSize);
			}
		} catch (IOException e) {
			Log.i("EtsyJsonHttpClient", "HTTP response cache installation failed:" + e);
		}
	}
	
	private static EtsyJsonHttpClient instance;
	public static EtsyJsonHttpClient getInstance(){
		if(instance == null){
			instance = new EtsyJsonHttpClient();
		}
		return instance;
	}
	
	public void fetch(String url, EtsyJsonHttpCallback callback) {
		EtsyJsonHttpClientRequest runnable = new EtsyJsonHttpClientRequest(url, callback);
		pool.execute(runnable);
	}

	class EtsyJsonHttpClientRequest extends Handler implements Runnable{
		String uri;
		EtsyJsonHttpCallback callback;
		JSONObject jsonObject;
		public EtsyJsonHttpClientRequest(String uri, EtsyJsonHttpCallback callback){
			this.uri = uri;
			this.callback = callback;
		}
		
		public JSONObject get(String uri) throws JSONException, IOException{
			URL url = new URL(uri);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			String response = readStream(con.getInputStream());
			return new JSONObject(response);
		}
	
		private String readStream(InputStream in) {
			StringBuffer buff = new StringBuffer();
			BufferedReader reader = null;
			try {
				reader = new BufferedReader(new InputStreamReader(in));
				String line = "";
				while ((line = reader.readLine()) != null) {
					buff.append(line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (reader != null) {
					try {
						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			return buff.toString();
		}

		@Override
		public void run() {
			try {
				jsonObject = get(uri);
				Message completeMessage = obtainMessage(0, this);
				completeMessage.sendToTarget();
			} catch (JSONException e) {
				e.printStackTrace();
				Message errorMessage = obtainMessage(1, e);
				errorMessage.sendToTarget();
			} catch (IOException e) {
				e.printStackTrace();
				Message errorMessage = obtainMessage(1, e);
				errorMessage.sendToTarget();
			}
		}
		
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 0:
				callback.callback(false, jsonObject, null);
				break;
			case 1:
				callback.callback(true, null, (Exception) msg.obj);
			}
		}
	}
}
