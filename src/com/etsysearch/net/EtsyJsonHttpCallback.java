package com.etsysearch.net;

import org.json.JSONObject;

public abstract class EtsyJsonHttpCallback {
    public abstract void callback(boolean isError, JSONObject jsonObject, Exception exception);
}
