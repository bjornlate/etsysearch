package com.etsysearch;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.etsysearch.net.EtsyJsonHttpCallback;
import com.etsysearch.net.EtsyJsonHttpClient;
import com.squareup.picasso.Picasso;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * A fragment representing a single SearchResult detail screen. This fragment is
 * either contained in a {@link SearchResultListActivity} in two-pane mode (on
 * tablets) or a {@link SearchResultDetailActivity} on handsets.
 */
public class SearchResultDetailFragment extends Fragment {
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_JSON = "item-json";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private JSONObject itemJson;
	private JSONObject listingJson;
	
	private View progressView;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public SearchResultDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_ITEM_JSON)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			try {
				itemJson = new JSONObject(getArguments().getString(ARG_ITEM_JSON));
				
				String url = "https://api.etsy.com/v2/listings/"+itemJson.optString("listing_id")+"?api_key=liwecjs0c3ssk6let4p1wqt9&includes=Images";
				
				EtsyJsonHttpClient client = EtsyJsonHttpClient.getInstance();
				client.fetch(url, new EtsyJsonHttpCallback() {
					
					@Override
					public void callback(boolean isError, JSONObject jsonObject,
							Exception exception) {
						//Filth
						listingJson = jsonObject.optJSONArray("results").optJSONObject(0);
						fetchImages();
					}
				});
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.view_etsy_listing_detail,
				container, false);

		// Show the dummy content as text in a TextView.
		if (itemJson != null) {
			
			TextView tv = (TextView) view.findViewById(R.id.ssli_title);
			if(tv != null){
				String title = itemJson.optString("title", "<title>");
				tv.setText(Html.fromHtml(title));
			}
			
			tv = (TextView) view.findViewById(R.id.ssli_subtitle);
			if(tv != null){
				String title = "$" + itemJson.optString("price", "<price>");
				tv.setText(Html.fromHtml(title));
			}
			
			tv = (TextView) view.findViewById(R.id.ssli_categories);
			if(tv != null){
				JSONArray categories = itemJson.optJSONArray("category_path");
				if(categories != null){

					StringBuffer categoriesStr = new StringBuffer();
					for(int i=0;i<categories.length();++i){
						categoriesStr.append(categories.optString(i));
						if(i < categories.length() -1){
							categoriesStr.append(" | ");
						}
					}
					
					tv.setText(Html.fromHtml(categoriesStr.toString()));
				}
			}
			
			
			tv = (TextView) view.findViewById(R.id.quantity);
			if(tv != null){
				int quantity = itemJson.optInt("quantity", 0);
				String text = getString(R.string.quantity, Integer.valueOf(quantity));
				tv.setText(text);
			}
			
			ImageView icon = (ImageView) view.findViewById(R.id.ssli_icon);
			if(icon != null){
				JSONObject mainImage = itemJson.optJSONObject("MainImage");
				if(mainImage != null){
					String iconUrl = mainImage.optString("url_170x135");
					if(!iconUrl.isEmpty()){
						Picasso.with(view.getContext()).load(iconUrl).into(icon);
					}
				}
			}	
			
			tv = (TextView) view.findViewById(R.id.tags);
			if(tv != null){
				JSONArray categories = itemJson.optJSONArray("tags");
				if(categories != null){

					StringBuffer categoriesStr = new StringBuffer();
					for(int i=0;i<categories.length();++i){
						categoriesStr.append(categories.optString(i));
						if(i < categories.length() -1){
							categoriesStr.append(" | ");
						}
					}
					
					tv.setText(Html.fromHtml(categoriesStr.toString()));
				}			
			}
			
			tv = (TextView) view.findViewById(R.id.description);
			if(tv != null){
				tv.setText(Html.fromHtml(itemJson.optString("description")));
			}
			
			progressView = view.findViewById(R.id.progressContainer);
			if(progressView != null){
				progressView.setVisibility(View.VISIBLE);
				View progressSpinner = view.findViewById(R.id.progressSpinner);
				if(progressSpinner != null){
					progressSpinner.animate();
				}
			}
		}

		return view;
	}
	
	private void fetchImages(){
		JSONArray images = listingJson.optJSONArray("Images");
		if(images == null){
			return;
		}
		
		LinearLayout imagesView = (LinearLayout) this.getActivity().findViewById(R.id.images);
		if(imagesView == null){
			return;
		}
		/**
		 * Add ImageViews for each image, add them to the HSV,
		 * fetch each image using Picasso.
		 */
		for(int i=0;i<images.length();++i){
			JSONObject imageJson = images.optJSONObject(i);
			if(imageJson != null){
				ImageView imageView = (ImageView) LayoutInflater.from(getActivity()).inflate(R.layout.view_etsy_listing_image, null);
				imagesView.addView(imageView);
				Picasso.with(getActivity()).load(imageJson.optString("url_570xN")).into(imageView);
			}
		}
		
		/**
		 * Show the images container
		 * would be nice to wire this to the appearance of one of the images, unfortunately
		 * ImageView doesn't have a listener option for that...
		 */
		if(progressView != null){
			progressView.setVisibility(View.GONE);
			View imagesScroll = this.getActivity().findViewById(R.id.image_scroll);
			if(imagesScroll != null){
				imagesScroll.setVisibility(View.VISIBLE);
			}
		}
	}

	public String getShareUrl() {
		if(itemJson != null){
			return itemJson.optString("url");
		}
		return "";
	}

	public void sendShareIntent() {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("text/plain");
		String title = itemJson.optString("title");
		//TODO: convert these to substituted string resources..
		i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_subject, title));
		i.putExtra(Intent.EXTRA_TEXT, getShareUrl());
		startActivity(Intent.createChooser(i, getString(R.string.share_chooser_title, title)));
	}
	
}
